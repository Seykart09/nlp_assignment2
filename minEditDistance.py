import numpy

def sub_cost(source, target):
    if source != target:
        value = 2
    else:
        value = 0

    return value

def minEditDistance(source, target):
    n = len(source)
    m = len(target)

    del_cost = 1
    ins_cost = 1

    #Create a distance matrix distance[n+1,m+1]
    D = numpy.zeros(shape = (n,m))

    # Initialization: the zeroth row and column is the distance from the empty string
    # D[0][0] = 0
    for i in range(1, n):
        D[i][0] = D[i-1][0] + del_cost
        print(i)

    for j in range(1, m):
        D[0][j] = D[0][j-1] + ins_cost


    # Recurrence relation:
    for i in range(1, n):
        for j in range (1, m):
            D[i][j] = min(D[i-1][j] + del_cost,
                          D[i-1][j-1] + sub_cost(source[i], target[j]),
                          D[i][j-1] + ins_cost)

    print(D)

    return D[n-1][m-1]







print(minEditDistance("execution", "intention"))
