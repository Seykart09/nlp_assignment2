
# coding: utf-8

# In[35]:


import numpy


# In[36]:


def sub_cost(source, target):
    if source != target:
        value = 2
    else:
        value = 0

    return value


# In[37]:


def minEditDistance(source, target):
    n = len(source)
    m = len(target)

    del_cost = 1
    ins_cost = 1

    #Create a distance matrix distance[n+1,m+1]
    D = numpy.zeros(shape = (n+1,m+1))

    # Initialization: the zeroth row and column is the distance from the empty string
    # D[0][0] = 0
    for i in range(1, n+1):
        D[i][0] = D[i-1][0] + del_cost

    for j in range(1, m+1):
        D[0][j] = D[0][j-1] + ins_cost


    # Recurrence relation:
    for i in range(1, n+1):
        for j in range (1, m+1):
            D[i][j] = min(D[i-1][j] + del_cost,
                          D[i-1][j-1] + sub_cost(source[i-1], target[j-1]),
                          D[i][j-1] + ins_cost)

    print(D)
    return D[n-1][m-1]


# In[38]:


print(minEditDistance("execution", "intention"))

